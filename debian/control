Source: dt-utils
Section: kernel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: dpkg-dev (>= 1.22.5), debhelper-compat (= 13), pkg-config, libfdt-dev, libudev-dev
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/dt-utils
Vcs-Git: https://salsa.debian.org/debian/dt-utils.git

Package: dt-utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Device tree and barebox related tools
 dt-utils holds dtblint for device tree linting and barebox-state
 for controlling the barebox bootloader's state information.

Package: libdt-utils5t64
Provides: ${t64:Provides}
Replaces: libdt-utils5
Breaks: libdt-utils5 (<< ${source:Version})
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Device tree related library
 libdt-utils provides the foundation for dt-utils' tools.
 It provides a programmer with an API to analyze
 device tree files and is an alternative to libfdt.

Package: libdt-utils-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libdt-utils5t64 (= ${binary:Version})
Description: Device tree related library (development files)
 libdt-utils provides the foundation for dt-utils' tools.
 It provides a programmer with an API to analyze
 device tree files and is an alternative to libfdt.
 .
 This package holds the development files for the library.
